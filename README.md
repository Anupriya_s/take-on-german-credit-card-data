# Take on German credit card data

#exploratory analysis 

*  to understand the data, Data Cleansing, Data Preparation, Analyzing data, creating dummy variables, splitting data into training and test, Model
development, predicting new result, validating with the actual results, checking model
accuracy and optimizing the model.

#Algorithms used
*  Logistic regression
*  Random forrest
*  Decision trees
*  Kmeans clustering

#Accuracy
*  Logistic regression- 73.5%
*  Random forrest- 75%
*  Decision trees- 65%

Hence, from the above analysis i conclude that Random forrest algorithm has better accuracy when compared to others for this data set.