# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 20:24:55 2020

@author: smart
"""

#importing the necessary libraries
import pandas as pd  
import numpy as np  
import matplotlib.pyplot as plt  
from sklearn import metrics
from sklearn.model_selection import train_test_split 
from sklearn.linear_model import LogisticRegression
import seaborn as sns
from sklearn import metrics


dataset = pd.read_csv(r'D:\INTROTALLENT\Python\Project-datasets\german_data.csv',encoding = "ISO-8859-1")
#to check is there are any null values in the dataset
dataset.isnull().sum()
#after running the above line, its noted that there are no null values.
#hence no missing values treatement is done on the dataset

"""
crisp-dm implementation

"""

"""
1. data understanding
"""

# pairplot to see how the data is panned
sns.pairplot(dataset)


# Density plot with histograms of column values for each numerical column 
# before outlier treatment

# Duration in month vs Credit amount
sns.catplot(x="Duration in month", y="Credit amount", data=dataset);

# purpose vs Credit amount
sns.catplot(x="Purpose", y="Credit amount", data=dataset);

# Installment rate in percentage of disposable income vs Credit amount
sns.catplot(x="Installment rate in percentage of disposable income", y="Credit amount", data=dataset);

# Present residence since vs Credit amount
sns.catplot(x="Present residence since", y="Credit amount", data=dataset, height=8, aspect=0.9);

# Age in Years since vs Credit amount
sns.catplot(x="Age in Years", y="Credit amount", data=dataset, height=12, aspect=0.9);

# Default_status since vs Credit amount
sns.catplot(x="Default_status", y="Credit amount", data=dataset);


#to plot a graph Duration vs Credit amount, to see how the data is panned out
dataset.plot(x='Duration in month', y='Credit amount', style='o')  
plt.title('Duration vs Credit amount')  
plt.xlabel('Duration')  
plt.ylabel('Credit amount')  
plt.show()

male_credit = dataset["Credit amount"].loc[dataset["Personal status and sex"].str.contains('A91|A93|A94')].values
female_credit = dataset["Credit amount"].loc[dataset["Personal status and sex"].str.contains('A92|A95')].values
total_credit = dataset['Credit amount'].values


fig, ax = plt.subplots(1, 3, figsize=(16,4))

sns.distplot(male_credit, ax=ax[0], color="#FE642E")
ax[0].set_title("Male Credit Distribution", fontsize=16)
sns.distplot(female_credit, ax=ax[1], color="#F781F3")
ax[1].set_title("Female Credit Distribution", fontsize=16)
sns.distplot(total_credit, ax=ax[2], color="#2E64FE")
ax[2].set_title("Total Credit Distribution", fontsize=16)
plt.show()

dataset['Age_Group'] = ""

lst = [dataset]

for col in lst:
    col.loc[(col['Age in Years'] > 18) & (col['Age in Years'] <= 29), 'Age_Group'] = 'Young'
    col.loc[(col['Age in Years'] > 29) & (col['Age in Years'] <= 40), 'Age_Group'] = 'Young Adults'
    col.loc[(col['Age in Years'] > 40) & (col['Age in Years'] <= 55), 'Age_Group'] = 'Senior'
    col.loc[col['Age in Years'] > 55, 'Age_Group'] = 'Elder' 
    
dataset.head()

sns.catplot(x="Age_Group", y="Credit amount", data=dataset);

"""
2. data preparation
"""

#to check if there are any outliers
# def to check the outliers in the df
def ro(d,c):
    q1=d[c].quantile(0.25)
    q3=d[c].quantile(0.75)
    iqr=q3-q1
    ub=q3+(1.58*iqr)
    lb=q1-(1.58*iqr)
    f1=d[c].quantile(0.05)
    f2=d[c].quantile(0.95)
    d[c][d[c]<lb]=f1
    d[c][d[c]>ub]=f2
    return d[c]

'''
outlier treatment
'''
dataset['Duration in month'].quantile(0.92)
dataset['Duration in month'][dataset['Duration in month'] > 
                             dataset['Duration in month'].quantile(0.92)] = dataset['Duration in month'].quantile(0.92)
ro(dataset, 'Duration in month')
plt.boxplot(dataset['Duration in month'])


# dataset['Credit amount'].quantile(0.93)
# dataset['Credit amount'][dataset['Credit amount'] > dataset['Credit amount'].quantile(0.92)] = dataset['Credit amount'].quantile(0.92)
# ro(dataset, 'Credit amount')
# plt.boxplot(dataset['Credit amount'])

ro(dataset, 'Installment rate in percentage of disposable income')
plt.boxplot(dataset['Installment rate in percentage of disposable income'])

ro(dataset, 'Present residence since')
plt.boxplot(dataset['Present residence since'])

dataset['Age in Years'].quantile(0.97)
dataset['Age in Years'][dataset['Age in Years'] > dataset['Age in Years'].quantile(0.97)] = dataset['Age in Years'].quantile(0.97)
ro(dataset, 'Age in Years')
plt.boxplot(dataset['Age in Years'])



'''
3. modelling
'''

#the y dataframe has only default status column
y = dataset.iloc[:,20]

#x df has every column expect the default status column
X = dataset[['Age in Years', 'Job_status', 'Personal status and sex', 'Savings account/bonds',
             'Status of existing checking account', 'Credit amount',
             'Duration in month', 'Purpose',
             'Credit history']]

X1=X
X1

# X_numeric_col has numerical values from x1 df 
numerics = ['int64']
X_numeric_col = X1.select_dtypes(include=numerics)
X_numeric_col

# X_obj_col has non numerical values ie object values of X1
X_obj_col = X1.select_dtypes(include=object)
X_obj_col

#transfer all non numerical to numerical- use label encoding on x4
from sklearn import preprocessing

le = preprocessing.LabelEncoder()
# 2/3. FIT AND TRANSFORM
# use df.apply() to apply le.fit_transform to all columns
X4 = X_obj_col.apply(le.fit_transform)
X4

#combine cleaned data 
#x_clean_data has all the column in encoded format expect the target variable
X_clean_data=X4.join(X_numeric_col)
X_clean_data.dtypes

y

#modelling
#split for training and testing 
X_train, X_test, y_train, y_test = train_test_split(X_clean_data, y, test_size=0.2, random_state=0)

#training the algorithm
#logistic regression 
regressor = LogisticRegression()
regressor.fit(X_train, y_train)

#to rpedict
y_pred_log_reg_test = regressor.predict(X_test)
y_pred_log_reg_test

y_train.shape

y_pred_log_reg_train = regressor.predict(X_train)
y_pred_log_reg_train

sns.catplot(x=y_pred_log_reg_test, y=y_pred_log_reg_train);

print(regressor.score(X_test,y_test))
#score 0.735

#for confusion matrix, accuracy, classification report

from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score 
from sklearn.metrics import classification_report 

confusion_matrix=confusion_matrix(y_test,y_pred_log_reg_test.round())
print(confusion_matrix)

accuracy_log_reg = accuracy_score(y_test,y_pred_log_reg_test.round())
print(accuracy_log_reg)

classification_log_reg = classification_report(y_test,y_pred_log_reg_test.round())
print(classification_log_reg)

#roc curve
probs = regressor.predict_proba(X_test)
preds = probs[:,1]
fpr, tpr, threshold =metrics.roc_curve(y_test,y_pred_log_reg_test.round())

roc_auc_log_reg = metrics.auc(fpr,tpr)
#roc_auc_log_reg = 0.6878338999514326


import matplotlib.pyplot as plt

plt.subplots(1, figsize=(10,10))
plt.title('German Credit Data - Logistic regression')
plt.plot(fpr,tpr)
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()

#find the error obtained 

print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred_log_reg_test))  
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred_log_reg_test))  
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred_log_reg_test)))


"""
Mean Absolute Error: 0.265
Mean Squared Error: 0.265
Root Mean Squared Error: 0.51478150704935

logestic regression

"""
##########################################################################################

"""
random forest implementation

"""

from sklearn.ensemble import RandomForestClassifier 

# fit, train and cross validate RandomForestClassifier with training and test data 
randomforest = RandomForestClassifier().fit(X_train, y_train)
print(randomforest,'\n')
    

# Probabilities for each class
rf_probs = randomforest.predict_proba(X_test)[:, 1]
rf_probs


# Predict target variables y for test data
# Actual class predictions
y_pred_ran_for = randomforest.predict(X_test)
y_pred_ran_for

y_pred_ran_for_train = randomforest.predict(X_train)
y_pred_ran_for_train

print(randomforest.score(X_test,y_test))
#score 0.725

from sklearn.metrics import confusion_matrix

confusion_matrix_ran_for=confusion_matrix(y_test,y_pred_ran_for.round())
print(confusion_matrix_ran_for)

accuracy_ran_for = accuracy_score(y_test,y_pred_ran_for.round())
print(accuracy_ran_for)

classification_ran_for = classification_report(y_test,y_pred_ran_for.round())
print(classification_ran_for)

#roc curve
fpr_rf, tpr_rf, threshold_rf =metrics.roc_curve(y_test,y_pred_ran_for.round())

roc_auc_rf = metrics.auc(fpr_rf,tpr_rf)
#roc_auc_log_reg = 0.6317387081107334

#plotting
plt.subplots(1, figsize=(10,10))
plt.title('German Credit Data - Random forest')
plt.plot(fpr_rf,tpr_rf)
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()

#find the error obtained 

print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred_ran_for))  
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred_ran_for))  
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred_ran_for)))

"""
Mean Absolute Error: 0.245
Mean Squared Error: 0.245
Root Mean Squared Error: 0.4949747468305833
Random forest
"""
#############################################################################
"""
#decision tree

"""
from sklearn import tree

decisiontree= tree.DecisionTreeClassifier(criterion='gini')

decisiontree.fit(X_train, y_train.ravel())

train_score_decision_tree=decisiontree.score(X_train,y_train)
train_score_decision_tree
#train_score_decision_tree---- 1.0

# to predict the x test and train models
y_predicted_test = decisiontree.predict(X_test)
y_predicted_test

y_predicted_train = decisiontree.predict(X_train)
y_predicted_train

test_decisiontree_score = decisiontree.score(X_test,y_test)
test_decisiontree_score
#test_decisiontree_score---------- 0.635

print("Accuracy:",metrics.accuracy_score(y_test, y_predicted_test))
#Accuracy: 0.695

from sklearn.metrics import confusion_matrix


con_mat_dt=confusion_matrix(y_test,y_predicted_test.round())
print(con_mat_dt)

classification_dt = classification_report(y_test,y_predicted_test.round())
print(classification_dt)

#roc curve
fpr_dt, tpr_dt, threshold_dt =metrics.roc_curve(y_test,y_predicted_test.round())

roc_auc_dt = metrics.auc(fpr_dt,tpr_dt)
#roc_auc_log_reg = 0.6181398737251093

#plotting
plt.subplots(1, figsize=(10,10))
plt.title('German Credit Data - decision tree')
plt.plot(fpr_dt,tpr_dt)
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from sklearn import tree

tree.plot_tree(decisiontree.fit(X_train, y_train.ravel())) 




#find the error obtained 

print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_predicted_test))  
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_predicted_test))  
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_predicted_test)))


"""
decision tree

Mean Absolute Error: 0.305
Mean Squared Error: 0.305
Root Mean Squared Error: 0.552268050859363

"""
#############################################################################

"""
knn 
"""

from sklearn.neighbors import KNeighborsClassifier
from sklearn import utils
from sklearn.cluster import KMeans

X_ktrain, X_ktest, y_ktrain, y_ktest = train_test_split(X_clean_data, y, random_state=1)

knn = KNeighborsClassifier(n_neighbors=5, metric='euclidean')

lab_enc = preprocessing.LabelEncoder()
training_scores_encoded = lab_enc.fit_transform(y_ktrain)
print(training_scores_encoded)
print(utils.multiclass.type_of_target(y_ktrain))
print(utils.multiclass.type_of_target(y_ktrain.astype('int')))
print(utils.multiclass.type_of_target(training_scores_encoded))

knn.fit(X_ktrain, training_scores_encoded)

y_pred = knn.predict(X_test)
y_pred

sns.scatterplot(
    x=y_ktest['Credit Amount'],
    y=y_pred,
    hue='benign',
    data=X_ktest.join(y_ktest, how='outer')
)

plt.scatter(
    X_ktest['Credit history'],
    X_ktest['Purpose'],
    c=y_pred,
    cmap='coolwarm',
    alpha=0.7
)

x = X_numeric_col.iloc[:,:].values


sse = []
for i in range(1, 11):
    kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=300, n_init=10, random_state=0)
    kmeans.fit(x)
    sse.append(kmeans.inertia_)

plt.plot(range(1, 11), sse)
plt.title('Elbow Method')
plt.xlabel('Number of clusters')
plt.ylabel('SSE')
plt.show()

# Number of clusters
kmeans = KMeans(n_clusters=4)


# Fitting the input data
kmeans = kmeans.fit(x)
print(kmeans)

# Getting the cluster labels
labels = kmeans.predict(x)
print(labels)



print(kmeans.predict(x))


# Centroid values
centers = kmeans.cluster_centers_
print(centers)

plt.scatter(x[ : , 0], x[ : , 1], s =50)

plt.show()

#to plot the scatter plot for the kmeans 
plt.scatter(x[:,0], x[:,1], c=labels)

#the cluster centers obtained
plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5);


#chi squared test
import pandas as pd

#Validate whether Neighborhood and Brick are independent of each other (2-tailed)

contingency_table=pd.crosstab(dataset["Credit amount"],dataset["Default_status"])
print(contingency_table)
from scipy.stats import chi2_contingency

chi2_contingency(contingency_table)

stat, p, dof, expected = chi2_contingency(contingency_table)

print(p)#pvalue
print(dof)
if p <= 0.05:
	print('Dependent (reject H0)')
else:
	print('Independent (fail to reject H0)')

'''
0.3865967855326766
920
Independent (fail to reject H0)
'''
'''
t test
'''